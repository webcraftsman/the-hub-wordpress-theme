<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?>">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<div id="news-listing">
	<?php wp_reset_postdata(); ?>
		<?php
			$the_query = new WP_Query( array(
			'post_type' => 'news_stories',
			'order' => 'DESC'
			));
		?>
		<?php if( $the_query->have_posts() ): ?>
		<?php while( $the_query->have_posts() ) : $the_query->the_post();?>
			<article class="listing">
				<div class="wrapper">
					<figure>
						<img src="<?php the_field('image'); ?>" alt="" />
					</figure>
					<p class="post-date"><?php the_time('m.d.Y'); ?></p>
					<h3 class="listing-title"><?php the_title(); ?></h3>
					<?php if (get_field('is_link')): ?>
						<p><?php the_field('link_teaser');?></p>
						<p><a href="<?php the_field('link_url');?>" target="_blank" rel="noopener">Find out more</a></p>
					<?php else : ?>
						<div class="content"><?php the_field('show_content'); ?></div>
						<?php if (get_field('more_content')): ?>
						<div class="more-content"><?php the_field('more_content');?></div>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</article>
		<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	</div>
	<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>