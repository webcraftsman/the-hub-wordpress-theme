<?php

/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  */
// ENQUE SCRIPTS AND CSS
/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---  */

function hub_styles() {
	$base = get_template_directory_uri();
	wp_enqueue_style('screen', get_template_directory_uri().'/_css/main.css','','1.0');
	wp_enqueue_style('screen', get_template_directory_uri().'/_css/jquery.fullPage.css','','1.0');
}
add_action('wp_enqueue_scripts','hub_styles');

function hub_scripts() {
  $base = get_template_directory_uri();
  wp_enqueue_script('jquery');
  wp_enqueue_script('modernizer', $base . '/_js/modernizer.js',array('jquery'), '1.0.0');
  wp_enqueue_script('picturefill', $base . '/_js/picturefill.min.js',array('jquery'), '1.0.0');
  wp_enqueue_script('slick', $base . '/_js/slick.min.js',array('jquery'), '1.0.0', true);
  wp_enqueue_script('fullPage', $base . '/_js/jquery.fullPage.min.js',array('jquery'), '1.0.0', true);
  wp_enqueue_script('easings', $base . '/_js/jquery.easings.min.js',array('jquery'), '1.0.0', true);
  wp_enqueue_script('smoothState', $base . '/_js/jquery.smoothState.min.js',array('jquery'), '1.0.0', true);

  if (is_singular('available_units') || is_page('amenities')) {
		wp_enqueue_script('waypoints', $base.'/_js/jquery.waypoints.min.js',array('jquery'), '1.0.0', true);
  }
   if (is_page('news')) {
		wp_enqueue_script('masonry', $base.'/_js/masonry.min.js',array('jquery'), '1.0.0', true);
  }
  if (is_page('contact')) {
		wp_enqueue_script('mousewheel', $base.'/_js/vendor/jquery.mousewheel.js',array('jquery'), '1.0.0', true);
		wp_enqueue_script('mwheelintent', $base.'/_js/vendor/mwheelIntent.js',array('jquery'), '1.0.0', true);
		wp_enqueue_script('scrollpane', $base.'/_js/vendor/jquery.jscrollpane.min.js',array('jquery'), '1.0.0', true);
		wp_enqueue_script('contact', $base.'/_js/contact.js',array('jquery'), '1.0.0', true);
  }
  wp_enqueue_script('main', $base . '/_js/main.min.js',array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts','hub_scripts');

function admin_scripts() {
  wp_register_style('admin_css', get_bloginfo('stylesheet_directory' ).'/stylesheets/admin.css', false, '1.0.0');
  wp_enqueue_style('admin_css');
}
add_action('admin_enqueue_scripts', 'admin_scripts');


function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

function remove_menus(){
//	remove_menu_page( 'index.php' );
	remove_menu_page( 'edit.php' );
	remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails', array( 'post' ) );
	add_image_size('dots', 312, 160, array('center', 'center'));
	add_image_size('avail-thumb', 604, 287, array('center', 'center'));
	add_image_size('1208-1x', 1208, 9999);
	add_image_size('1080-1x', 1080, 9999);
	add_image_size('864-1x', 864, 9999);
	add_image_size('640-1x', 640, 9999);
	add_image_size('tablet-retina', 1536, 9999);
	add_image_size('tablet-no-retina', 768, 9999);
	add_image_size('sm-tablet-retina', 1200, 9999);
	add_image_size('sm-tablet-no-retina', 600, 9999);
	add_image_size('mobile-retina', 960, 9999);
	add_image_size('mobile-no-retina', 480, 9999);
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyDEPEr9KLsxiA3zY-MbwP9pr5T4jjcKsUw');
}

add_action('acf/init', 'my_acf_init');

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

?>
