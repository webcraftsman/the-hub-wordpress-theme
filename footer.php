<?php
	$phone = get_field('phone_number', 'option');
	$phoneTrimmed = preg_replace("/[^0-9]/", "", $phone);
?>
<nav id="site-nav" role="navigation" aria-hidden="false">
	<h1 class="logo" aria-hidden="true">Hub</h1>
	<span class="close-button"></span>
	<ol class="nav-main">
		<li class="nav-building"><a href="/building/">Building</a></li>
		<li class="nav-amenities"><a href="/amenities">Amenities</a></li>
		<li class="nav-views"><a href="/views/">Views</a></li>
		<li class="nav-residences"><a href="/apartments/">Apartments</a></li>
		<li class="nav-availability"><a href="/availability">Availability</a></li>
		<li class="nav-neighborhood"><a href="/downtown-brooklyn">Downtown Brooklyn</a></li>
		<li class="nav-team"><a href="/developer/">Developer</a></li>
		<li class="nav-apply"><a href="https://www.on-site.com/apply/property/205193" target="_blank">Application</a></li>
		<li class="nav-news"><a href="/news/">News</a></li>
		<li class="nav-contact"><a href="/contact/">Contact</a></li>
	</ol>
	<div class="content-secondary">
		<ol class="mobile-contact mobile">
			<li class="nav-call"><a href="tel:+1<?php echo $phoneTrimmed; ?>">Call</a></li>
			<li class="nav-email"><a href="mailto:<?php the_field('email_address','option'); ?>">Email</a></li>
			<li class="nav-map"><a href="<?php the_field('map_link','option'); ?>" target="_blank">Map</a></li>
		</ol>
		<div class="contact-information hcard tablet">
			<span class="adr">
			<span class="street-address"><?php the_field('street_address','option'); ?></span>
			<span class="locality">Brooklyn</span>, <span class="region">NY</span></span>
			<span class="email"><a href="mailto:<?php the_field('email_address','option'); ?>"><?php the_field('email_address','option'); ?></a></span>
			<span class="tel"><a href="tel:+1<?php echo $phoneTrimmed; ?>"><?php the_field('phone_number', 'option');?></a></span>
		</div>
		<div class="legal"><a href="/legal-and-privacy/">Legal &amp; Privacy</a></div>
	</div>
</nav>
<footer role="contentinfo">
	<ol>
		<li class="street-address"><a href="<?php the_field('map_link','option'); ?>" target="_blank"><?php the_field('street_address','option'); ?>, Brooklyn, NY</a></li>
		<li class="contact-email"><a href="mailto:<?php the_field('email_address','option'); ?>"><?php the_field('email_address','option'); ?></a></li>
		<li class="privacy-link"><a href="/legal-and-privacy/">Legal &amp; Privacy</a></li>
</footer>
<div id="privacy-modal">
	<span class="close-btn">Close</span>
	<?php wp_reset_postdata(); ?>
	<?php
		$the_query = new WP_Query( array(
		'page_id' => '28'
		));
	?>
	<?php if( $the_query->have_posts() ): ?>
		<?php while( $the_query->have_posts() ) : $the_query->the_post();?>
		<div class="privacy-content">
		<?php the_content();?>
		</div>
		<?php endwhile; endif;?>

</div>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869727734;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/869727734/?guid=ON&amp;script=0"/>
</div>
</noscript>
<?php wp_footer(); ?>
