<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> main-layout-page two-tier">
	<?php include '_includes/banner.php'; ?>
	<?php if ( have_posts() ): ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="layout-container">
			<?php include '_includes/main-layout.php'; ?>
			<div id="content-secondary" class="section" data-anchor="at-a-glance">
				<div id="amenity-illustration">
					<h2 class="section-heading"><?php the_field('section_heading');?></h2>
					<div id="amenities-slider">
						<figure id="great-lawn" class="slide">
							<img src="<?php the_field('great_lawn_image'); ?>" alt="" />
							<figcaption><?php the_field('great_lawn_caption'); ?></figcaption>
						</figure>
						<figure id="pool" class="slide">
							<img src="<?php the_field('pool_image'); ?>" alt="" />
							<figcaption><?php the_field('pool_caption'); ?></figcaption>
						</figure>
						<figure id="sun-terrace" class="slide">
							<img src="<?php the_field('sun_terrace_image'); ?>" alt="" />
							<figcaption><?php the_field('sun_terrace_caption'); ?></figcaption>
						</figure>
						<figure id="fitness-center" class="slide">
							<img src="<?php the_field('fitness_center_image'); ?>" alt="" />
							<figcaption><?php the_field('fitness_center_caption'); ?></figcaption>
						</figure>
						<figure id="party-room" class="slide">
							<img src="<?php the_field('party_room_image'); ?>" alt="" />
							<figcaption><?php the_field('party_room_caption'); ?></figcaption>
						</figure>
						<figure id="screening-rooms" class="slide">
							<img src="<?php the_field('screening_rooms_image'); ?>" alt="" />
							<figcaption><?php the_field('screening_rooms_caption'); ?></figcaption>
						</figure>
						<figure id="grilling-cabanas" class="slide">
							<img src="<?php the_field('grilling_cabanas_image'); ?>" alt="" />
							<figcaption><?php the_field('grilling_cabanas_caption'); ?></figcaption>
						</figure>
						<figure id="dog-run" class="slide">
							<img src="<?php the_field('dog_run_image'); ?>" alt="" />
							<figcaption><?php the_field('dog_run_caption'); ?></figcaption>
						</figure>
						<figure id="play-room" class="slide">
							<img src="<?php the_field('play_room_image'); ?>" alt="" />
							<figcaption><?php the_field('play_room_caption'); ?></figcaption>
						</figure>
						<figure id="sky-lounge" class="slide">
							<img src="<?php the_field('sky_lounge_image'); ?>" alt="" />
							<figcaption><?php the_field('sky_lounge_caption'); ?></figcaption>
						</figure>
					</div>
					<div class="amenities-arrows">
						<span class="arrow-left" data-dir="left">Back</span>
						<span class="arrow-right" data-dir="right">Forward</span>
					</div>
					<ol id="amenities-slider-nav">
						<li class="nav-item"><a href="#great-lawn">great lawn</a><span>, </span></li>
						<li class="nav-item"><a href="#pool">pool</a><span>, </span></li>
						<li class="nav-item"><a href="#sun-terrace">sun terrace</a><span>, </span></li>
						<li class="nav-item"><a href="#fitness-center">fitness center</a><span>, </span></li>
						<li class="nav-item"><a href="#party-room">party rooms</a><span>, </span></li>
						<li class="nav-item"><a href="#screening-rooms">screening rooms</a><span>, conference center, game room, </span></li>
						<li class="nav-item"><a href="#grilling-cabanas">grilling cabanas</a><span>, </span></li>
						<li class="nav-item"><a href="#dog-run">dog run</a><span>, bike storage, </span></li>
						<li class="nav-item"><a href="#play-room">play room</a><span>, </span></li>
						<li class="nav-item"><span>and topped off with a magnificent </span><a href="#sky-lounge">Sky Lounge</a><span> on the 53rd floor.</span></li>
					</ol>
				</div>
				<ol class="slide-nav bottom-section">
					<li><a href="#slider-content" id="view-gallery">Gallery</a></li>
					<li><span>At a Glance</span></li>
				</ol>
			</div>
		</div>
		<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
	<?php endif; ?>
</body>
</html>
