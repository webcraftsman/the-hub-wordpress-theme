<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> main-layout-page two-tier">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="layout-container">
			<?php include '_includes/main-layout.php'; ?>
			<div id="content-secondary" class="section" data-anchor="view-map">
				<div class="map-container">
					<div class="map-content">
						<h2><?php the_field('map_section_header'); ?></h2>
						<div class="map-teaser"><?php the_field('map_section_intro_text'); ?></div>
						<div class="map">
							<img src="<?php the_field('map_image');?>" />
						</div>
						<div id="map-accordion" class="map-accordion">
							<div class="accordion boerum-hill">
								<h3 class="accordion-header">Boerum Hill</h3>
								<div class="accordion-content">
								<?php if (have_rows('boerum_hill_locations')): ?>
									<ol>
									<?php while (have_rows('boerum_hill_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion brooklyn-heights">
								<h3 class="accordion-header">Brooklyn Heights</h3>
								<div class="accordion-content">
								<?php if (have_rows('brooklyn_heights_locations')): ?>
									<ol>
									<?php while (have_rows('brooklyn_heights_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion cobble-hill">
								<h3 class="accordion-header">Cobble Hill</h3>
								<div class="accordion-content">
								<?php if (have_rows('cobble_hill_locations')): ?>
									<ol>
									<?php while (have_rows('cobble_hill_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>	
								</div>
							</div>
							<div class="accordion carroll-gardens">
								<h3 class="accordion-header">Carroll Gardens</h3>
								<div class="accordion-content">
								<?php if (have_rows('carroll_gardens_locations')): ?>
									<ol>
									<?php while (have_rows('carroll_gardens_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion fort-greene">
								<h3 class="accordion-header">Fort Greene</h3>
								<div class="accordion-content">
								<?php if (have_rows('fort_green_locations')): ?>
									<ol>
									<?php while (have_rows('fort_green_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion navy-yard">
								<h3 class="accordion-header">Navy Yard</h3>
								<div class="accordion-content">
								<?php if (have_rows('navy_yard_locations')): ?>
									<ol>
									<?php while (have_rows('navy_yard_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion dumbo">
								<h3 class="accordion-header">Dumbo</h3>
								<div class="accordion-content">
								<?php if (have_rows('dumbo_locations')): ?>
									<ol>
									<?php while (have_rows('dumbo_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion park-slope">
								<h3 class="accordion-header">Park Slope</h3>
								<div class="accordion-content">
								<?php if (have_rows('park_slope_locations')): ?>
									<ol>
									<?php while (have_rows('park_slope_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
							<div class="accordion prospect-heights">
								<h3 class="accordion-header">Prospect Heights</h3>
								<div class="accordion-content">
								<?php if (have_rows('prospect_heights_locations')): ?>
									<ol>
									<?php while (have_rows('prospect_heights_locations')) : the_row();?>
										<li><?php the_sub_field('location_name');?></li>
									<?php endwhile; ?>
									</ol>
								<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<ol class="slide-nav bottom-section">
					<li><a href="#slider-content" id="view-gallery">Gallery</a></li>
					<li><span>Map</span></li>
				</ol>
			</div>
		</div>
		<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>