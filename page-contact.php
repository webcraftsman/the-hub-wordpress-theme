<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?>">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="content-main">
			<div id="contact">
				<?php
				//Set Your Nonce
				$ajax_nonce = wp_create_nonce( "contact_form_nonce" );
				?>
				<script type="text/javascript">
					function sendContactForm(data){
						var payload = {
							values: data,
							security: '<?php echo $ajax_nonce; ?>'
						}
						jQuery.ajax({
							url : "/wp-admin/admin-ajax.php?action=contact_form",
							type: "POST",
							data : payload,
							success: function(data, textStatus, jqXHR){
								window.location.href = "/thanks";
							},
							error: function (jqXHR, textStatus, errorThrown){
							}
						});
					}
				</script>
				<div id="contact-fields" data-fields="name:required|email:required|phone|type:required|price:required|date|found" data-fields-mobile="name-mobile:required|email-mobile:required|phone-mobile|type-mobile:required|price-mobile:required|date-mobile|found-mobile">
					<span>Hello, I'm</span>

					<span class="fun-field">
						<div class="desktop">
							<span tabindex="1" class="text" id="name" data-id="name" data-field-description="Full Name"></span>
							<label>name<sup>*</sup></label>
						</div>
						<div class="mobile">
							<input type="text" name="fullName" id="name-mobile" data-id="name-mobile" placeholder="first and last name*" />
						</div>
					</span><span><span class="desktop-comma">,</span> email me at</span>

					<span class="fun-field">
						<div class="desktop">
							<span tabindex="2" class="text" id="email" data-id="email" data-field-description="Email Address"></span>
							<label>email<sup>*</sup></label>
						</div>
						<div class="mobile">
							<input type="email" name="email" id="email-mobile" data-id="email-mobile" placeholder="email*"/>
						</div>
					</span> <span>or call my cell,</span>

					<span class="fun-field has-period">
						<div class="desktop">
							<span tabindex="3" class="text has-period" id="phone" data-id="phone" data-field-description="Phone Number"></span>
							<label>phone number</label>
						</div>
						<div class="mobile">
							<input type="text" name="phone" id="phone-mobile" data-id="phone-mobile" placeholder="phone number"/>
						</div>
					</span> <span>I'm interested in a </span>

					<span class="fun-field">
						<div class="desktop">
							<span tabindex="4" class="text" id="type" data-id="type" data-field-description="Residence Type"></span>
							<label>residence type<sup>*</sup></label>
							<ul>
								<li>studio</li>
								<li>one bedroom</li>
								<li>two bedroom</li>
								<li>penthouse</li>
							</ul>
						</div>
						<div class="mobile">
							<span class="text" id="type-mobile" data-id="type-mobile" data-field-description="Residence Type"></span>
							<label>residence type<sup>*</sup></label>
							<select name="residenceType" id="mobile-residence-range" placeholder="residence type" multiple="multiple">
								<option value="" disabled selected>residence type*</option>
								<option value="studio">studio</option>
								<option value="one bedroom">one bedroom</option>
								<option value="two bedroom">two bedroom</option>
								<option value="penthouse">penthouse</option>
							</select>
						</div>
					</span> <span>for about</spa>

					<span class="fun-field">
						<div class="desktop">
							<span tabindex="5" class="text" id="price" data-id="price" data-field-description="Price Range"></span>
							<label>price range<sup>*</sup></label>
							<ul>
								<li>$2,000–$2,500</li>
								<li>$2,500–$3,000</li>
								<li>$3,000–$3,500</li>
								<li>$3,500–$4,000</li>
								<li>$4,000–$4,500</li>
								<li>$4,500–$5,000</li>
								<li>$5,000–$5,500</li>
								<li>$5,500–$6,000</li>
								<li>$6,000–$6,500</li>
								<li>$6,500–$7,000</li>
								<li>$7,000–$7,500</li>
								<li>$7,500–$8,000</li>
								<li>$8,000+</li>
							</ul>
						</div>
						<div class="mobile">
							<span class="text" id="price-mobile" data-id="price-mobile" data-field-description="Price Range"></span>
							<label>price range<sup>*</sup></label>
							<select name="priceRange" id="mobile-price-range" placeholder="price range" multiple="multiple">
								<option value="" disabled selected>price range*</option>
								<option value="$2,000–$2,500" class="mp-option">$2,000–$2,500</option>
								<option value="$2,500–$3,000" class="mp-option">$2,500–$3,000</option>
								<option value="$3,000–$3,500" class="mp-option">$3,000–$3,500</option>
								<option value="$3,500–$4,000" class="mp-option">$3,500–$4,000</option>
								<option value="$4,000–$4,500" class="mp-option">$4,000–$4,500</option>
								<option value="$4,500–$5,000" class="mp-option">$4,500–$5,000</option>
								<option value="$5,000–$5,500" class="mp-option">$5,000–$5,500</option>
								<option value="$5,500–$6,000" class="mp-option">$5,500–$6,000</option>
								<option value="$6,000–$6,500" class="mp-option">$6,000–$6,500</option>
								<option value="$6,500–$7,000" class="mp-option">$6,500–$7,000</option>
								<option value="$7,000–$7,500" class="mp-option">$7,000–$7,500</option>
								<option value="$7,500–$8,000" class="mp-option">$7,500–$8,000</option>
								<option value="$8,000+" class="mp-option">$8,000+</option>
							</select>
						</div>
					</span> <span>and I plan to move around </span>

					<span class="fun-field has-period">
						<div class="desktop">
							<span tabindex="6" class="text has-period" id="date" data-id="date" data-field-description="Calendar"></span>
							<label>calendar</label>
							<ul>
								<li>January</li>
								<li>February</li>
								<li>March</li>
								<li>April</li>
								<li>May</li>
								<li>June</li>
								<li>July</li>
								<li>August</li>
								<li>September</li>
								<li>October</li>
								<li>November</li>
								<li>December</li>
							</ul>
						</div>
						<div class="mobile">
							<span class="text" id="date-mobile" data-id="date-mobile" data-field-description="Calendar"></span>
							<label>calendar</label>
							<select name="calendar" id="mobile-calendar-range" multiple="multiple">
								<option value="" disabled selected>calendar</option>
								<option value="January">January</option>
								<option value="February">February</option>
								<option value="March">March</option>
								<option value="April">April</option>
								<option value="May">May</option>
								<option value="June">June</option>
								<option value="July">July</option>
								<option value="August">August</option>
								<option value="September">September</option>
								<option value="October">October</option>
								<option value="November">November</option>
								<option value="December">December</option>
							</select>
						</div>
					</span><span> I found Hub via</span>

					<span class="fun-field">
						<div class="desktop">
							<span tabindex="7" class="text" id="found" data-id="found" data-field-description="How did you hear about us?"></span>
							<label>how did you hear about us?</label>
							<ul>
								<li>StreetEasy</li>
								<li>Web Search</li>
								<li>Referral</li>
								<li>Walk-By</li>
								<li>News</li>
								<li>Other</li>
							</ul>
						</div>
						<div class="mobile">
							<span class="text" id="found-mobile" data-id="found-mobile" data-field-description="How did you hear about us?"></span>
							<label>How did you hear about us?</label>
							<select name="found" id="mobile-found-range" multiple="multiple">
								<option value="" disabled selected>how did you hear about us?</option>
								<option value="StreetEasy">StreetEasy</option>
								<option value="Web Search">Web Search</option>
								<option value="Referral">Referral</option>
								<option value="Walk-By">Walk-By</option>
								<option value="News">News</option>
								<option value="Other">Other</option>
							</select>
						</div>
					</span><span><span class="desktop-comma">,</span> and look forward to hearing from you soon.</span>

					<p>
						<label>required fields<sup>*</sup></label>
						<input tabindex="8" type="button" value="submit" id="thebutton">
					</p>
				</div>
			</div>

		</div>
	</div>
	<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>
