<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> floor-plans">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="unit-information">
			<?php if( have_rows('add_images') ):?>
			<div id="unit-slider" class="unit-slider">
			<?php while( have_rows('add_images')) : the_row(); ?>
			<?php $image = get_sub_field('image');
			      if ( !empty($image) ):
			      $url = $image['url'];
			      $thumb = $image['sizes']['dots'];
			      $noRetina = $image['sizes']['1208-1x'];
			      $tabletRetina = $image['sizes']['tablet-retina'];
				  $tabletNoRetina = $image['sizes']['tablet-no-retina'];
				  $smTabletRetina = $image['sizes']['sm-tablet-retina'];
				  $smTabletNoRetina = $image['sizes']['sm-tablet-no-retina'];
				  $mobileRetina = $image['sizes']['mobile-retina'];
				  $mobileNoRetina = $image['sizes']['mobile-no-retina'];
			?>
				<figure class="slide">
					<div class="unit-image-container"> 
					<img src="<?php echo $mobileNoRetina; ?>" data-background="<?php echo $noRetina;?>" 
						sizes="(min-width: 37.5em) calc(100vw - 72px),
							   calc(100vw - 36px)"
						srcset="<?php echo $mobileNoRetina; ?> 480w,
								<?php echo $mobileRetina;?> 960w,
								<?php echo $smTabletNoRetina;?> 600w,
								<?php echo $smTabletRetina;?> 1200w,
								<?php echo $tabletNoRetina;?> 768w,
								<?php echo $tabletRetina; ?> 1536w,
								<?php echo $noRetina; ?> 1208w,
								<?php echo $url; ?> 2416w"
						alt="<?php the_sub_field('image_caption'); ?>" />
					</div>
					<figcaption><?php the_sub_field('image_caption'); ?></figcaption>
				</figure>
			
			<?php endif; ?>
			<?php endwhile; ?>
			</div>
			<?php endif; ?>
			<div class="unit-container">
				<div class="apartment-info">
					<h2 class="unit-title"><?php the_title(); ?></h2>
					<?php $bedrooms = get_field('bedrooms');
						$bedroom = 'bedroom';
						if ($bedrooms === '2') {
							$bedroom = 'bedrooms';
						}
						
						$bathrooms = get_field('bathrooms');
						$bathroom = 'bathroom';
						if ($bathrooms === '2') {
							$bathroom = 'bathrooms';
						}
					?>
					<div class="unit-size"><?php echo $bedrooms; ?> <?php echo$bedroom; ?>, <?php echo $bathrooms; ?> <?php echo $bathroom; ?></div>
				</div>
				<div class="apartment-pricing">
				<?php if (get_field('available_now')): ?>
					<div class="unit-price"><span>Available Now</span> <br/>from $<?php the_field('price'); ?><sup>*</sup></div>
					<div class="unit-special">*<?php the_field('pricing_explanation'); ?></div>
				<?php else: ?>
					<div class="unit-price"><span class="availability-date"><?php if (get_field('availability_date')):?><?php echo the_field('availability_date');?><?php endif;?></span><br/>from $<?php the_field('price'); ?></div>
				<?php endif; ?>
				</div>
				<div class="unit-description"><?php the_field('apartment_description'); ?></div>
				<?php if (get_field('floor_plan_image_furniture')) :?>
				<div class="unit-floor-plan" data-furniture="<?php the_field('floor_plan_image_furniture');?>">
					<div class="image">
						<img class="no-furniture" src="<?php the_field('floor_plan_image');?>" alt="" />
					</div>
				</div>
				<?php else :?>
				<div class="unit-floor-plan">
					<div class="image">
						<img class="no-furniture" src="<?php the_field('floor_plan_image');?>" alt="" />
					</div>
				</div>
				<?php endif; ?>
				<div class="unit-download-plan"><a href="<?php the_field('floor_plan_download.pdf'); ?>" target="_blank">Download Floor Plan</a></div>
			
				<div class="back-link"><a href="/availability/">Back to Availability</a></div>
			</div>
		</div>
		<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>