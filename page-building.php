<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> main-layout-page">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php include '_includes/main-layout.php'; ?>
		<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>