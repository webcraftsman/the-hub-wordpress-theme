<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> single-page">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="content-main">
			<?php the_content();?>
		
		</div>
	</div>
	<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>