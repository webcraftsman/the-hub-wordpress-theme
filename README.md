# Live Site

You can view the live site at https://hubbk.com

# Functionality

The site uses [fullpage.js](https://alvarotrigo.com/fullPage) on homepage for smaller screens and all pages on larger screens. Advanced Customs Fields and Custom Post Type UI plugins were used for CMS functionality.

