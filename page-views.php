<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> main-layout-page">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="main-layout">
		<div class="content-container">
			<div id="slider-content">
				<div class="the-slider">
				<?php $firstImage = get_field('first_image');
					if ( !empty($firstImage) ):
					  $firsturl = $firstImage['url'];
					  $firsttabletRetina = $firstImage['sizes']['tablet-retina'];
				      $firsttabletNoRetina = $firstImage['sizes']['tablet-no-retina'];
				      $firstsmTabletRetina = $firstImage['sizes']['sm-tablet-retina'];
				      $firstsmTabletNoRetina = $firstImage['sizes']['sm-tablet-no-retina'];
				      $firstnoRetina = $firstImage['sizes']['1208-1x'];
				      $firstmobileRetina = $firstImage['sizes']['mobile-retina'];
				      $firstmobileNoRetina = $firstImage['sizes']['mobile-no-retina'];
				    endif;
				?>
				<article class="slide first-slide">
					<div class="introduction">
						<h2 class="page-title"><?php the_title(); ?></h2>
						<p class="page-description"><?php the_field('page_content');?></p>
					</div>
					<figure>
						<div class="image-container">
							<img src="<?php echo $firstmobileNoRetina; ?>" data-background="<?php echo $firstnoRetina;?>"
							sizes="(min-width: 37.5em) calc(100vw - 72px),
								   calc(100vw - 36px)"
							srcset="<?php echo $firstmobileNoRetina; ?> 480w,
									<?php echo $firstmobileRetina;?> 960w,
									<?php echo $firstsmTabletNoRetina;?> 600w,
									<?php echo $firstsmTabletRetina;?> 1200w,
									<?php echo $firsttabletNoRetina;?> 768w,
									<?php echo $firsttabletRetina; ?> 1536w,
									<?php echo $firstnoRetina; ?> 1208w,
									<?php echo $firsturl; ?> 2416w"
							alt="<?php the_field('first_image_caption'); ?>" />
						</div>
						<figcaption><?php the_field('first_image_caption'); ?></figcaption>
					</figure>
				</article>	
				<?php if( have_rows('add_images') ):?>
					<?php while( have_rows('add_images')) : the_row(); ?>
					<?php $image = get_sub_field('image');
					      if ( !empty($image) ):
					      $url = $image['url'];
					      $thumb = $image['sizes']['dots'];
					      $noRetina = $image['sizes']['1208-1x'];
					      $tabletRetina = $image['sizes']['tablet-retina'];
						  $tabletNoRetina = $image['sizes']['tablet-no-retina'];
						  $smTabletRetina = $image['sizes']['sm-tablet-retina'];
						  $smTabletNoRetina = $image['sizes']['sm-tablet-no-retina'];
						  $mobileRetina = $image['sizes']['mobile-retina'];
						  $mobileNoRetina = $image['sizes']['mobile-no-retina'];
					?>
						<article class="slide">
							<figure>
								<div class="image-container"> 
								<img src="<?php echo $mobileNoRetina; ?>" data-background="<?php echo $noRetina;?>" 
									sizes="(min-width: 37.5em) calc(100vw - 72px),
										   calc(100vw - 36px)"
									srcset="<?php echo $mobileNoRetina; ?> 480w,
											<?php echo $mobileRetina;?> 960w,
											<?php echo $smTabletNoRetina;?> 600w,
											<?php echo $smTabletRetina;?> 1200w,
											<?php echo $tabletNoRetina;?> 768w,
											<?php echo $tabletRetina; ?> 1536w,
											<?php echo $noRetina; ?> 1208w,
											<?php echo $url; ?> 2416w"
									alt="<?php the_sub_field('image_caption'); ?>" />
								</div>
								<figcaption><?php the_sub_field('image_caption'); ?></figcaption>
							</figure>
						</article>
					<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
				
			</div>
		</div>
		<ol class="slide-nav top-section">
			<li><span>Gallery</span></li>
			<li><a href="/apartments">Apartments</a></li>
		</ol>
	<?php endwhile; // end of the loop. ?>
	</div>
	<?php get_footer(); ?>
</body>
</html>