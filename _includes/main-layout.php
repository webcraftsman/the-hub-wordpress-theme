<div class="main-layout section" data-anchor="first-section">
	<div class="content-container">
		<div id="slider-content">
		<!--	<span class="slide-back"></span> -->
				<div class="the-slider">
				<?php $firstImage = get_field('first_image');
					if ( !empty($firstImage) ):
					  $firsturl = $firstImage['url'];
					  $firsttabletRetina = $firstImage['sizes']['tablet-retina'];
				      $firsttabletNoRetina = $firstImage['sizes']['tablet-no-retina'];
				      $firstsmTabletRetina = $firstImage['sizes']['sm-tablet-retina'];
				      $firstsmTabletNoRetina = $firstImage['sizes']['sm-tablet-no-retina'];
				      $firstnoRetina = $firstImage['sizes']['1208-1x'];
				      $firstmobileRetina = $firstImage['sizes']['mobile-retina'];
				      $firstmobileNoRetina = $firstImage['sizes']['mobile-no-retina'];
				    endif;
				?>
				<article class="slide first-slide">
					<div class="introduction">
						<h2 class="page-title"><?php the_title(); ?></h2>
						<p class="page-description"><?php the_field('page_content');?></p>
						<?php if (is_page('Amenities')): ?>
							<a href="#content-secondary" id="scroll-link" class="scroll-link">At a Glance</a>
						<?php elseif (is_page('Downtown Brooklyn')): ?>
							<a href="#content-secondary" id="scroll-link" class="scroll-link">View the Map</a>
						<?php endif; ?>
					</div>
					<figure>
						<div class="image-container">
							<img src="<?php echo $firstmobileNoRetina; ?>" data-background="<?php echo $firstnoRetina;?>"
							sizes="(min-width: 37.5em) calc(100vw - 72px),
								   calc(100vw - 36px)"
							srcset="<?php echo $firstmobileNoRetina; ?> 480w,
									<?php echo $firstsmTabletNoRetina;?> 600w,
									<?php echo $firsttabletNoRetina;?> 768w,
									<?php echo $firstmobileRetina;?> 960w,
									<?php echo $firstsmTabletRetina;?> 1200w,
									<?php echo $firstnoRetina; ?> 1208w,
									<?php echo $firsttabletRetina; ?> 1536w,
									<?php echo $firsturl; ?> 2416w"
							alt="<?php the_field('first_image_caption'); ?>" />
						</div>
						<figcaption><?php the_field('first_image_caption'); ?></figcaption>
					</figure>
				</article>
				<?php if( have_rows('add_images') ):?>
				<?php while( have_rows('add_images')) : the_row(); ?>
					<?php $image = get_sub_field('image');
					  $ratio = get_sub_field('image_aspect_ratio');
				      if ( !empty($image) ):
				      $url = $image['url'];
				      if ( $ratio === 'ratio1'):
					      $noRetina = $image['sizes']['864-1x'];
					      $w = '864w';
					      $w2 = '1728w';
				      elseif( $ratio === 'ratio2'):
					      $noRetina = $image['sizes']['640-1x'];
					      $w = '640w';
					      $w2 = '1280w';
				      elseif( $ratio === 'ratio3'):
					      $noRetina = $image['sizes']['1080-1x'];
					      $w = '1080w';
					      $w2 = '2160w';
				      elseif( $ratio === 'ratio4'):
					      $noRetina = $image['sizes']['1208-1x'];
					      $w = '1208w';
					      $w2 ='2416w';
				      endif;
				      $tabletRetina = $image['sizes']['tablet-retina'];
				      $tabletNoRetina = $image['sizes']['tablet-no-retina'];
				      $smTabletRetina = $image['sizes']['sm-tablet-retina'];
				      $smTabletNoRetina = $image['sizes']['sm-tablet-no-retina'];
				      $mobileRetina = $image['sizes']['mobile-retina'];
				      $mobileNoRetina = $image['sizes']['mobile-no-retina'];
				?>
				<article class="slide <?php echo $ratio; ?>">
					<figure>
						<div class="image-container">
							<img src="<?php echo $mobileNoRetina; ?>" data-background="<?php echo $noRetina;?>"
							sizes="(min-width: 37.5em) calc(100vw - 72px),
								   calc(100vw - 36px)"
							srcset="<?php echo $mobileNoRetina; ?> 480w,
									<?php echo $mobileRetina;?> 960w,
									<?php echo $smTabletNoRetina;?> 600w,
									<?php echo $smTabletRetina;?> 1200w,
									<?php echo $tabletNoRetina;?> 768w,
									<?php echo $tabletRetina; ?> 1536w,
									<?php echo $noRetina; ?> <?php echo $w; ?>,
									<?php echo $url; ?> <?php echo $w2; ?>"
							alt="<?php the_sub_field('image_caption'); ?>" />
						</div>
						<figcaption><?php the_sub_field('image_caption'); ?></figcaption>
					</figure>
				</article>
				<?php endif; ?>
				<?php endwhile; ?>
				<?php endif; ?>
				</div>
		</div>
		<?php if (is_page('Apartments')): ?>
			<ol class="slide-nav top-section">
				<li><span>Gallery</span></li>
				<li><a href="/views">Views</a></li>
			</ol>
		<?php endif;?>
		<?php if (is_page('Amenities')): ?>
			<ol class="slide-nav top-section">
				<li><span>Gallery</span></li>
				<li><a href="#content-secondary" id="at-a-glance">At a Glance</a></li>
			</ol>
		<?php endif;?>
		<?php if (is_page('Downtown Brooklyn')): ?>
			<ol class="slide-nav top-section">
				<li><span>Gallery</span></li>
				<li><a href="#content-secondary" id="view-the-map">Map</a></li>
			</ol>
		<?php endif;?>
	</div>
</div>
