<header role="banner">
	<h1 id="site-logo"><a href="/">Hub</a></h1>
	<ol class="tablet nav-secondary">
		<li class="nav-availability"><a href="/availability/">Availability</a></li>
		<li class="nav-apply"><a href="https://www.on-site.com/apply/property/205193" target="_blank">Application</a></li>
		<li class="nav-contact"><a href="/contact/">Contact</a></li>
	</ol>
	<a href="#site-nav" id="menu-btn">Menu</a>
</header>