<div id="page-overlay" aria-hidden="true">
	<h2 class="page-title"><?php the_title(); ?></h2>
	<span id="close-modal">See More</span>
	<div class="hover-overlay">
		<p class="hover-description"><?php the_field('hover_content'); ?></p>
	</div>
	<?php include 'secondary-nav.php'; ?>
</div>