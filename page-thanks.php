<?php get_header(); ?>
<body id="page-<?php echo $post->post_name; ?>" class="page-<?php echo $post->post_name; ?>">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<section id="thanks">
	<span class="image-wrapper">
		<img src="<?php echo get_template_directory_uri(); ?>/_img/thanks-illustration.png"  />
	</span>
	<span class="content-wrapper">
		<div>
			<h1>Thanks for signing up</h1>
			<p>We'll be in touch.<br><br><br>xoxo<br><br>Hub</p>
		</div>
	</span>
	<div style="clear:both;"></div>
</section>
	</div>
	<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>


