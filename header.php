<!DOCTYPE html>
<html class="no-js<?php if( is_user_logged_in()) { echo ' logged-in'; } ?>" prefix="og: http://ogp.me/ns#">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" user-scalable="no" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<?php if (is_front_page()) :?>
			<title>Hub | Live High</title>
		<?php else :?>
			<title><?php echo bloginfo('name');?> | <?php wp_title('',true);?> </title>
		<?php endif; ?>
		<?php if (is_page('contact')) :?>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/_css/vendor/jquery.jscrollpane.css">
		<?php endif; ?>
		<?php wp_head(); ?>
		<?php if (is_page('thanks')) :?>
		<script>// Lead
		// fbq('track', 'Lead');
		</script>
		<?php endif; ?>
		<script src="//calls.onlinemarketinggrp.com/scripts/profile/55340.js"></script>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1840184719549224');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1840184719549224&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
	</head>
