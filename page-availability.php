<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?>">
	<?php include '_includes/banner.php'; ?>
	<div id="availability-content">
		<div id="availability-filter-mobile">
			<select>
				<option>Filter Apartments</option>
				<option value="all">All</option>
				<option value="studio">Studio</option>
				<option value="bedroom-1">1 Bedroom</option>
				<option value="bedroom-2">2 Bedroom</option>
			</select>
		</div>
		<div id="availability-filter">
			<h3>Filter:</h3>
			<ol>
				<li class="active"><span class="all">All</span></li>
				<li><span class="studio">Studio</span</li>
				<li><span class="bedroom-1">1 Bedroom</span></li>
				<li><span class="bedroom-2">2 Bedroom</span></li>
			</ol>
		</div>
		<?php while ( have_posts() ) : the_post(); ?>
		<div id="availability-listing"<?php if( get_field('promotional_tile') ) : echo " class='promo-title'"; endif; ?>>
			<?php $posts = get_field('units_to_show');
				if( $posts ) : ?>
				<?php foreach( $posts as $post):
					setup_postdata($post); ?>
				<?php if( get_field('apartment_studio')) :?>
				<article class="listing studio">
				<?php else: ?>
				<article class="listing bedroom-<?php the_field('bedrooms'); ?>">
				<?php endif; ?>
					<a href="<?php the_permalink(); ?>">
						<figure>
						<?php if (have_rows('add_images')):
							$image = get_field('add_images');
							$first_img = $image[0]['image']['sizes']['avail-thumb'];
						?>
						<img src="<?php echo $first_img; ?>" alt="" />
						<?php endif; ?>
						<?php if (get_field('special')): ?>
							<?php if (get_field('special_pricing_background')): ?>
							<span class="special" style="background-color: <?php the_field('special_pricing_background');?>">Special: <?php the_field('special_pricing_text'); ?></span>
							<?php else :?>
							<span class="special">Special: <?php the_field('special_pricing_text'); ?></span>
							<?php endif; ?>
						<?php endif; ?>
						</figure>
						<h3 class="listing-title"><?php the_title(); ?></h3>
						<?php if( get_field('apartment_studio')) :?>
						
						<?php else :?>
							<?php $bedrooms = get_field('bedrooms');
								$bedroom = 'bedroom';
								if ($bedrooms === '2') {
									$bedroom = 'bedrooms';
								}
								
								$bathrooms = get_field('bathrooms');
								$bathroom = 'bathroom';
								if ($bathrooms === '2') {
									$bathroom = 'bathrooms';
								}
							?>
						<?php endif; ?>
						<ol class="listing-details">
							<li class="listing-size">
							<?php if( get_field('apartment_studio')) :?>
								<br/>studio
							<?php else: ?>
								<?php echo $bedrooms; ?> <?php echo $bedroom; ?><br/><?php echo $bathrooms; ?> <?php echo $bathroom; ?>
							<?php endif; ?>
							</li>
							<li class="listing-price">
								<span><?php if (get_field('available_now')): ?>available now</span><?php elseif (get_field('availability_date')): ?><?php echo the_field('availability_date');?><?php endif; ?></span><br/>
								from $<?php the_field('price'); ?>
							</li>
						</ol>
					</a>
				</article>
			<?php endforeach; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			<?php if( get_field('promotional_tile') ): ?>
				<?php if ( get_field('promotional_tile_background')) : ?>
				<article class="listing promotional-tile studio bedroom-1 bedroom-2" style="background-image:url(<?php the_field('promotional_tile_background');?>);"> 
				<?php else : ?>
				<article class="listing promotional-tile">
				<?php endif; ?>
					<div class="promo-text"><?php the_field('promotional_field_content');?></div>
				</article>
			<?php endif; ?>
		</div>
	</div>
	<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>