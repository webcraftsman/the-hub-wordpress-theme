<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> single-page">
	<?php include '_includes/banner.php'; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="content-main">
			<h2><?php the_title(); ?></h2>
			<div class="team-content">
			<?php if( have_rows('team_member') ):
				while ( have_rows('team_member') ) : the_row();?>
				<div class="team-section">
					<h4><?php the_sub_field('team_role'); ?></h4>
					<h3><?php the_sub_field('team_name'); ?></h3>
					<div class="team-content"><?php the_sub_field('team_content'); ?></div>
				</div>
				<?php endwhile;
			endif; ?>
			</div>
		</div>
	<?php get_footer(); ?>
	<?php endwhile; // end of the loop. ?>
</body>
</html>
