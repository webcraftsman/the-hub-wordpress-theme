<?php get_header(); ?>
<body id="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?>">
	<?php include '_includes/banner.php'; ?>
	<div id="homepage-slide-container">
		<div role="main" class="home-section first-section section" data-anchor="top">
			<h1>Hub</h1>
			<h2><?php the_field('subheading'); ?></h2>
<!--			<div class="hcard">
				<span class="street-address"><?php the_field('street_address','option'); ?></span>
				<span class="locality">Brooklyn</span>, <span class="region">NY</span>
			</div> -->
			<a id="home-scroll" class="scroll-link" href="#building">Scroll Down</a>
			<?php if( get_field('promotional_banner') ): ?>
			<div class="promo-banner"><?php the_field('promotional_banner'); ?></div>
			<?php endif; ?>
		</div>
		<div class="nav-section home-section section section-building" data-anchor="building">
			<h3 class="page-name"><a href="/building/">Building</a></h3>
			<a class="see-more" href="/building/">See More</a>
			<div class="hover-overlay">
				<p class="hover-description"><?php the_field('building_hover_content'); ?></p>
			</div>
		</div>
		<div class="nav-section home-section section section-residences" data-anchor="apartments">
			<h3 class="page-name"><a href="/apartments/">Apartments</a></h3>
			<a class="see-more" href="/apartments/">See More</a>
			<div class="hover-overlay">
				<p class="hover-description"><?php the_field('residences_hover_content'); ?></p>
			</div>
		</div>
		<div class="nav-section home-section section section-amenities" data-anchor="amenities">
			<h3 class="page-name"><a href="/amenities/">Amenities</a></h3>
			<a class="see-more" href="/amenities/">See More</a>
			<div class="hover-overlay">
				<p class="hover-description"><?php the_field('amenities_hover_content'); ?></p>
			</div>
		</div>
		<div class="nav-section home-section section section-neighborhood" data-anchor="downtown-brooklyn">
			<h3 class="page-name"><a href="/downtown-brooklyn">Downtown <br/>Brooklyn</a></h3>
			<a class="see-more" href="/downtown-brooklyn">See More</a>
			<div class="hover-overlay">
				<p class="hover-description"><?php the_field('neighborhood_hover_content'); ?></p>
			</div>
		</div>
	</div>
	<ol id="home-dot-nav" class="dot-nav">
		<li data-menuanchor="top" class="nav-home active"><a href="#top">Home</a></li>
		<li data-menuanchor="building" class="nav-building"><a href="#building">Building</a></li>
		<li data-menuanchor="apartments" class="nav-residences"><a href="#apartments">Apartments</a></li>
		<li data-menuanchor="amenities" class="nav-amenities"><a href="#amenities">Amenities</a></li>
		<li data-menuanchor="downtown-brooklyn" class="nav-neighborhood"><a href="#downtown-brooklyn">Downtown Brooklyn</a></li>
	</ol> 
	<ol id="tablet-dot-nav" class="dot-nav">
		<li data-tablet-anchor="top" class="nav-home active"><a href="#top">Home</a></li>
		<li data-tablet-anchor="building" class="nav-building"><a href="#building">Building</a></li>
		<li data-tablet-anchor="apartments" class="nav-residences"><a href="#apartments">Apartments</a></li>
		<li data-tablet-anchor="amenities" class="nav-amenities"><a href="#amenities">Amenities</a></li>
		<li data-tablet-anchor="downtown-brooklyn" class="nav-neighborhood"><a href="#downtown-brooklyn">Downtown Brooklyn</a></li>
	</ol>
	<?php get_footer(); ?>
</body>
</html>