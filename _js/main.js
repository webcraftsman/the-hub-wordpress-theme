jQuery(function ($) {

	$('#menu-btn').on('click',function(e) {
		e.preventDefault();
	//	$('#site-nav').toggleClass('show');
		$('#site-nav').css('zIndex','10');
		$('#site-nav').animate({
			opacity: 1
  			}, 250, function() {

  		});
	});
	$('.close-button').on('click',function(e) {
		e.preventDefault();
	//	$('#site-nav').removeClass('show');
		$('#site-nav').animate({
			opacity: 0
  			}, 250, function() {
  				$('#site-nav').css('zIndex','1');
  		});
	});


	if ( $('#home-scroll').length )
	{
		$('#home-scroll').click(function(e) {
			e.preventDefault();

			var target = $('.section-building');
			$target = $(target);

			$('html, body').animate({
				'scrollTop': $target.offset().top - 40
			}, 900, 'swing');
		});
	}

	function smoothScroll() {
		$('#tablet-dot-nav li').on('click', function(e) {
			e.preventDefault();
			var target = $(this).data('tablet-anchor');
			$target = $('.home-section[data-anchor="' + target +'"');

			$('html, body').animate({
				'scrollTop': $target.offset().top
			}, 900, 'swing');
		});
	}
	function subnavHighlighting() {
		var size = $(window).width();
		var sectionId = '';
		var topOffset = 0;

		$('.home-section').each( function() {
			var thisTop = ($(window).scrollTop() - $(this).offset().top+70) * -1;
			var thisBottom = (($(window).scrollTop() - $(this).offset().top) * -1) + $(this).outerHeight();
			if (thisTop <= topOffset && thisBottom >= topOffset) {
				sectionId = $(this).data('anchor');
			}
		});

		$('#tablet-dot-nav a').each(function() {
			var navId = $(this).parent().data('tablet-anchor');
			if ( navId === sectionId ) {
				$(this).parent().addClass('active');
			} else {
				$(this).parent().removeClass('active');
			}
		});

	}

	if ( $('#tablet-dot-nav').length )
	{

		smoothScroll();
		$(window).on('scroll', function(){
			subnavHighlighting();
		});
	}


	// Accordions on Downtown Brooklyn page
	function slideContent() {
		var allPanels = $('.accordion-content').hide();
		$('.accordion-header').click(function() {
			allPanels.slideUp();
			if ($(this).parent().hasClass('toggled'))
			{
				$(this).parent().removeClass('toggled');
				$(this).next().slideUp("slow", function(){
				});
			}
			else {
				$('.accordion').removeClass('toggled');
				$(this).parent().addClass('toggled');
				$(this).next().slideDown("slow", function(){
				});

			}
			return false;
		});
	}


	if ( $('body').hasClass('downtown-brooklyn') )
	{
		slideContent();
	}

	// Availability Page Filter
	if ( $('body').hasClass('availability'))
	{
		if ( $('#availability-filter').length )
		{
			$('#availability-filter li span').on('click',function() {
				$('#availability-filter li span').each(function() {
					$(this).parent().removeClass('active');
				});
				$(this).parent().addClass('active');
				var hash = $(this).attr('class');
				$('.listing').fadeOut(250).delay(175);
	
				if ( hash == 'all')
				{
					$('.listing').each(function(fadeInDiv){
						$(this).delay(fadeInDiv * 100).fadeIn(250);
					});
				}
				else {
					$('.'+hash).each(function(fadeInDiv){
						$(this).delay(fadeInDiv * 100).fadeIn(250);
					});
				}
			});
		}
		
		if ( $('#availability-filter-mobile').length )
		{
			console.log('works');
			$('#availability-filter-mobile select').on('change',function() {
				var hash = $(this).val();
				if ( hash != 'Filter Apartments' )
				{
					$('.listing').fadeOut(250).delay(175);
		
					if ( hash == 'all')
					{
						$('.listing').each(function(fadeInDiv){
							$(this).delay(fadeInDiv * 100).fadeIn(250);
						});
					}
					else {
						$('.'+hash).each(function(fadeInDiv){
							$(this).delay(fadeInDiv * 100).fadeIn(250);
						});
					}
				}
			});
		}
	}

/*	$(function(){
	  'use strict';
	  var $page = $('#layout-container'),
	      options = {
	        debug: true,
	        prefetch: true,
	        cacheLength: 2,
	        onStart: {
	          duration: 250, // Duration of our animation
	          render: function ($container) {
	            // Add your CSS animation reversing class
	            $container.addClass('is-exiting');
	            // Restart your animation
	            smoothState.restartCSSAnimations();
	          }
	        },
	        onReady: {
	          duration: 0,
	          render: function ($container, $newContent) {
	            // Remove your CSS animation reversing class
	            $container.removeClass('is-exiting');
	            // Inject the new content
	            $container.html($newContent);
	          }
	        }
	      },
	      smoothState = $page.smoothState(options).data('smoothState');
	});
*/

	if ( $('body').hasClass('main-layout-page') )
	{
		var size = $(window).width();

		if ( size >= 768 )
		{
			if ( ! Modernizr.objectfit ) {
			  $('.image-container').each(function () {
			    var $container = $(this),
			        imgUrl = $container.find('img').data('background');
			    if (imgUrl) {
			      $container
			        .css('backgroundImage', 'url(' + imgUrl + ')')
			        .addClass('compat-object-fit');
			    }
			  });
			}
		}
	}

	if ( $('body').hasClass('floor-plans') )

	{
		var size = $(window).width();

		$('#unit-slider').slick({
			slide:'.slide',
			infinite: true,
	    	slidesToShow:1,
	    	slidesToScroll:1,
	    	autoplay:true,
	    	dots:true,
	    	arrows:false,
	    	speed:500,
	    	cssEase: 'linear',
	    	autoplaySpeed:5000,
	    	mobileFirst: true,
	    	responsive: [{
		    	breakpoint: 768,
		    	settings: {
			    	autoplay:false,
			    	dots:false,
			    	arrows: true
		    	}
	    	}]
		});
	}

	if ( $('body').hasClass('news') )
	{
		var toggleLink = '<div class="read-more">Read More</div>';


		$('.listing').each(function(){
			if ( $(this).find('.more-content').length == 1)
			{
				$(this).children('.wrapper').append(toggleLink).children('.more-content').toggle();
			}
		});


		$('.listing .read-more').click(function(){
			if ( $(this).text() == 'Read More' )
			{
				$('.listing .read-more').not(this).text('Read More').prev().hide();
				$(this).text('Read Less').prev().toggle();


			}
			else
			{
				$(this).text('Read More').prev().toggle();
			}


			$('#news-listing').masonry();
		});


		$('#news-listing').addClass('masonry');
		var $container = $('#news-listing').masonry({
			percentPostion: true,
			columnWidth: '.listing',
			itemSelector: '.listing'
		});

		$container.imagesLoaded().progress( function() {
			$container.masonry('layout');
		});
	}
})


function HubResize ( $, window ) {
	window.watchResize = function( callback )
	{
		var resizing;
		function done()
		{
			clearTimeout( resizing );
			resizing = null;
			callback();
		}
		$(window).resize(function(){
			if ( resizing )
			{
				clearTimeout( resizing );
				resizing = null;
			}
			resizing = setTimeout( done, 50 );
		});
		// init
		callback();
	};

	function amenitiesSlideshow() {
		var showTimer = setInterval(function() {
			$("#amenities-slider").showtimer();
			$("#amenities-slider-nav").navtimer();
		}, 5000);

		$('#amenities-slider figure:first').addClass('active');
		$('#amenities-slider-nav li:first').addClass('active');
		jQuery.fn.showtimer = function() {
			if(!$(this).children('figure:last-child').hasClass('active')){
				$(this).children('figure.active')
					.removeClass('active')
					.next('figure').addClass('active');
			}
			else {
				$(this).children('figure.active')
					.removeClass('active')
				.end().children("figure:first")
					.addClass('active');
			}
		}

		jQuery.fn.navtimer = function() {
			if(!$(this).children('li:last-child').hasClass('active')){
				$(this).children('li.active')
					.removeClass('active')
					.next('li').addClass('active');
			}
			else{
				$(this).children('li.active')
					.removeClass('active')
				.end().children("li:first")
					.addClass('active');
			}
		}

		$('.amenities-arrows span').click(function(e){
			var slide_direction = $(this).data('dir');
			if ( slide_direction==='right' ) {
				var slide_target=$('#amenities-slider figure.active').next('figure');
				var nav_target=$('#amenities-slider-nav li.active').next('li');
				if ( slide_target.length===0 ) {
					slide_target = $('#amenities-slider figure:first');
					nav_target = $('#amenities-slider-nav li:first');
				}
			}
			else if ( slide_direction==='left' ) {
				var slide_target=$('#amenities-slider figure.active').prev('figure');
				var nav_target=$('#amenities-slider-nav li.active').prev('li');
				if ( slide_target.length===0 ) {
					slide_target = $('#amenities-slider figure:last');
					nav_target = $('#amenities-slider-nav li:last');
				}
			}
			$('#amenities-slider-nav li, #amenities-slider figure').removeClass('active');
			$(nav_target).addClass('active');
			$(slide_target).addClass('active');
			clearInterval(showTimer);
			showTimer = setInterval(function() {
				$("#amenities-slider").showtimer();
				$("#amenities-slider-nav").navtimer();
			}, 5000);
		});

		$('#amenities-slider-nav a').click(function(e){
			e.preventDefault();
			var target = $(this).attr('href');
			$('#amenities-slider-nav li, #amenities-slider figure').removeClass('active');
			$(this).parent().addClass('active');
			$(target).addClass('active');
			clearInterval(showTimer);
			showTimer = setInterval(function() {
				$("#amenities-slider").showtimer();
				$("#amenities-slider-nav").navtimer();
			}, 5000);
		});
	}

	window.watchResize(function(){
		var size = $(window).width();
		var height = $(window).height();


		if ( $('body').hasClass('homepage') )
		{

			if ( $('html').hasClass('fp-enabled') )
			{
				if (size < 960 || height <= 768 )
				{
					$.fn.fullpage.destroy('all');
				}
			}

			else
			{
				if ( size >= 960 && height > 768) {
					$('#homepage-slide-container').fullpage({
						anchors: ['top', 'building', 'apartments', 'amenities', 'downtown-brooklyn'],
						menu: '#home-dot-nav',
						css3:false,
						scrollingSpeed: 700,
						easing: 'easeInQuad'
					});
				}
			}

			if ( size >= 960 )
			{
				$('.see-more').hover(function(){
					$(this).prev().children('a').css('textShadow','none');
				});
			}
		}

		if ( $('body').hasClass('amenities') )
		{
			if ( size < 960 )
			{
				$('#at-a-glance, #scroll-link, #view-gallery').unbind();

				$('#scroll-link').on('click', function(e){
					e.preventDefault();
					var target = this.hash;
					$target = $(target);

					$('html, body').stop().animate({
						'scrollTop': $target.offset().top - 40
					}, 900, 'swing');
				});

				if ( !$('#content-secondary').hasClass('slideshow-fired') )
				{
					var waypoints = $('#content-secondary').waypoint(function(direction) {
						$(this.element).addClass('slideshow-fired');
						amenitiesSlideshow();
						}, {offset: '80%'});
				}
			}

			else if ( size >= 960) {
				$('#scroll-link').unbind();
				$('#at-a-glance,#scroll-link').on('click',function(e){
					e.preventDefault();
					$('#content-secondary').addClass('show');
					$('footer').addClass('show');
					if ( !$('#content-secondary').hasClass('slideshow-fired') )
					{
						$('#content-secondary').addClass('slideshow-fired');
						amenitiesSlideshow();
					}
				});
				$('#view-gallery').on('click',function(e){
					e.preventDefault();
					$('#content-secondary').removeClass('show');
					$('footer').removeClass('show');
				});
			}
			else {}
		}


		if ( $('body').hasClass('downtown-brooklyn') )
		{
			if ( size < 960 )
			{
				$('#view-the-map, #scroll-link, #view-gallery').unbind();
				$('#scroll-link').on('click', function(e){
					e.preventDefault();
					var target = this.hash;
					$target = $(target);

					$('html, body').stop().animate({
						'scrollTop': $target.offset().top - 40
					}, 900, 'swing');
				});
			}
			else if ( size >= 960)
			{
				$('#scroll-link').unbind();
				$('#view-the-map,#scroll-link').on('click',function(e){
					e.preventDefault();
					$('#content-secondary').addClass('show');
					$('footer').addClass('show');
				});
				$('#view-gallery').on('click',function(e){
					e.preventDefault();
					$('#content-secondary').removeClass('show');
					$('footer').removeClass('show');
				});
			}
			else {}
		}
		if ( $('body').hasClass('apartments') )
		{
			if ( size >= 960) {
				$('#go-gallery').on('click', function(e){
						e.preventDefault();
						$('#slider-content').addClass('slide');
				});
			}
		}
		
/*		if ( $('body').hasClass('availability'))
		{
			if ( size < 768 && !$('#availability-filter').hasClass('selector-menu') )
			{
				$('#availability-filter').addClass('selector-menu');
			}
			else if  ( size >= 768 && $('#availability-filter').hasClass('selector-menu') )
			{
				$('#availability-filter').removeClass('selector-menu');
				$('#availability-filter h3').removeClass('toggled');
				$('#availability-filter ol').show();
				
				$('#availability-filter h3').on('click',function(e) {
					$(this).unbind();
				});
				$('#availability-filter li span').on('click',function(e) {
					$(this).unbind();
				});
			}
			
			if ( size < 768 && $('#availability-filter').hasClass('selector-menu'))
			{
				$('#availability-filter h3').on('click',function(e) {
					e.preventDefault();
					$('#availability-filter ol').slideToggle();
					$(this).toggleClass('toggled');
				});
				
				$('#availability-filter li span').on('click',function(e) {
					$('#availability-filter ol').hide();
					$('#availability-filter h3').removeClass('toggled');
				});
			}
		} */
		
		
		if ( $('body').hasClass('main-layout-page') )
		{
			if ( $('#slider-content').hasClass('slick-implemented') )
			{
				if ( size < 960 )
				{
					$('#slider-content .the-slider').slick('unslick').parent().addClass('slick-disabled');
				}
				else if ( size >= 960 && $('#slider-content').hasClass('slick-disabled'))
				{
					$('#slider-content .the-slider').on('init', function(event, slick){
						$('.slick-prev').css('visibility','hidden');
					});

					$('#slider-content .the-slider').on('afterChange', function(event, slide, index){
						if (index === 1)
						{
							$('.slick-prev').css('visibility','visible');
						}
						else if (index === 0)
						{
							$('.slick-prev').css('visibility','hidden');
						}
					});

					$('#slider-content .the-slider').slick({
						slide:'.slide',
						infinite: false,
				    	slidesToShow:1,
				    	slidesToScroll:1,
				    //	variableWidth:true,
				    	autoplay:false,
				    	dots:false,
				    	arrows:true,
				    	speed:500,
				    	cssEase: 'linear',
				    	autoplaySpeed:5000
					}).removeClass('slick-disabled');
				}
			}
			else
			{
				if ( size >= 960 )
				{
					$('#slider-content .the-slider').on('init', function(event, slick){
						$('.slick-prev').css('visibility','hidden');
					});

					$('#slider-content .the-slider').on('afterChange', function(event, slide, index){
						if (index === 1)
						{
							$('.slick-prev').css('visibility','visible');
						}
						else if (index === 0)
						{
							$('.slick-prev').css('visibility','hidden');
						}
					});
					$('#slider-content .the-slider').slick({
						slide:'.slide',
						infinite: true,
				    	slidesToShow:1,
				    	slidesToScroll:1,
				    	variableWidth:true,
				    	autoplay:false,
				    	dots:false,
				    	arrows:true,
				    	speed:500,
				    	cssEase: 'linear',
				    	autoplaySpeed:5000
					});
					$('#slider-content').addClass('slick-implemented');
				}

			}

		}

		if ( $('body').hasClass('news') )
		{
			if ( size >= 600 && !$('#news-listing').hasClass('masonry'))
			{
				$('#news-listing').addClass('masonry');
				var $container = $('#news-listing').masonry({
					percentPostion: true,
					columnWidth: '.listing',
					itemSelector: '.listing'
				});

				$container.imagesLoaded().progress( function() {
					$container.masonry('layout');
				});
			}
			else if ( size < 600 && $('#news-listing').hasClass('masonry') )
			{
				$('#news-listing').removeClass('masonry');
				$container.masonry('destroy');
			}
		}
		
		if ( $('body').hasClass('floor-plans') )
		{
			if ( size >= 768 )
			{
				if ( $('.unit-floor-plan').attr('data-furniture') )
				{
					if ( !$('.unit-floor-plan').hasClass('changer-active'))
					{
						$('.unit-floor-plan').addClass('changer-active');
						changeFloorPlan();
					}
				}
			}
			
			function changeFloorPlan() {
				var planChanger = '<div class="plan-changer">Furniture <span class="on">On</span> / <span class="off active">Off</span></div>';
				var furniture = $('.unit-floor-plan').data('furniture');
				var furnitureImage = '<img class="furniture" src="' + furniture + '" alt="" />';
				$('.unit-floor-plan .image').append(furnitureImage).parent().append(planChanger);
	
				$('.plan-changer .on').click(function(){
					if ( !$(this).hasClass('active') )
					{
						$(this).addClass('active');
						$('.plan-changer .off').removeClass('active');
						$('.furniture').addClass('show');
						$('.no-furniture').addClass('hide');
					}
				});
	
				$('.plan-changer .off').click(function(){
					if ( !$(this).hasClass('active') )
					{
						$(this).addClass('active');
						$('.plan-changer .on').removeClass('active');
						$('.furniture').removeClass('show');
						$('.no-furniture').removeClass('hide');
					}
				});
			}
		}

		if ( size >= 768 )
		{
			$('.privacy-link a, .legal a').click(function(e){
				e.preventDefault();
				$('body').addClass('overflow-hide');
				$('#privacy-modal').addClass('show');
			});

			$('#privacy-modal .close-btn').on('click', function(){
				$('#privacy-modal').removeClass('show');
				$('body').removeClass('overflow-hide');
			});
		}
		else {
			$('.privacy-link a, .legal a').unbind('click');
		}
	});
}
HubResize( jQuery, window );
