(function($){

	var openFunFieldOptions = function(e){
		e.stopPropagation();
		$(this).removeClass('error');
		$(this).siblings('label').removeClass('error');
		$('ul').not($(this).parent().find('ul')).removeClass('open');
		$('ul',$(this).parent()).addClass('open');
		$('ul',$(this).parent()).jScrollPane({
			showArrows: false,
			horizontalGutter: 28,
			verticalGutter: 28,
			arrowScrollOnHover: true,
			verticalDragMinHeight: 16,
			verticalDragMaxHeight: 16
		});
	};
	

	var closeFunFieldOptions = function(){
		$('.fun-field ul').each(function(){
			$(this).removeClass('open');
		});
	};

	$('#arrow-more, #arrow-join-priority').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		var section = $($(this).attr('href'));
		$('html,body').animate({scrollTop:section.offset().top},500);
		return false;
	});

	$('#footer-legal').click(function(){
		window.scrollTo(0,document.body.scrollHeight);
		$('body').addClass('oh');
		$('#disclaimer').addClass('open');
	});

	$('#close-legal').click(function(){
		$('#disclaimer').removeClass('open');
		$('body').removeClass('oh');
	});

	$(document).ready(function(){

		$('#mobile-price-range').change(function(){
			var dis='', options = [], front, back;
			$("option:selected",$(this)).each(function(){
				if (this.value.length > 0) {
					options.push(this.value);
				}
			});
			if (options.length > 0) {
				dis = options.join(', ');
				if (options[0].indexOf('-') > -1) {
					front = options[0].split('-')[0];
					back = (options[options.length-1].indexOf('-') > -1) ? options[options.length-1].split('-')[1] : options[options.length-1].replace('$','');
					dis = front+'-'+back;
				}
				if (options[0].indexOf('–') > -1) {
					front = options[0].split('–')[0];
					back = (options[options.length-1].indexOf('–') > -1) ? options[options.length-1].split('–')[1] : options[options.length-1].replace('$','');
					dis = front+'–'+back;
				}
			}
			$('span',$(this).closest('.fun-field')).each(function(){
				$(this).text(dis).removeClass('error');
			});
			if( dis.length > 0 ) {
				$('label',$(this).closest('.fun-field')).hide();
			} else {
				$('label',$(this).closest('.fun-field')).show();
			}
		});

		$('#mobile-residence-range').change(function(){
			var options = [];
			$("#mobile-residence-range option:selected").each(function(){
				if (this.value.length > 0) {
					options.push(this.value);
				}
			});
			var i, dis='', usebdr = false;
			for ( i in options ) {
				if (options[i].indexOf('bedroom') > -1) {
					usebdr = true;
				}
			}
			if(usebdr) {
				var newopts = [];
				var ph = null;
				for ( i in options ) {
					if (options[i].indexOf('penthouse') < 0) {
						newopts.push(options[i].replace(' bedroom',''));
					} else {
						ph = 'ph';
					}
				}
				dis = newopts.join(', ') + ' bedroom' + ( ph ? ', ph' : '');
			}
			$('span',$(this).closest('.fun-field')).each(function(){
				$(this).text(dis).removeClass('error');
			});
			if( dis.length > 0 ) {
				$('label',$(this).closest('.fun-field')).hide();
			} else {
				$('label',$(this).closest('.fun-field')).show();
			}
		});

		$('#mobile-calendar-range, #mobile-found-range').change(function(){
			var options = [];
			$("option:selected",$(this)).each(function(){
				if (this.value.length > 0) {
					options.push(this.value);
				}
			});
			$('span',$(this).closest('.fun-field')).each(function(){
				$(this).text(options.join(', ')).removeClass('error');
			});
			if( options.length > 0 ) {
				$('label',$(this).closest('.fun-field')).hide();
			} else {
				$('label',$(this).closest('.fun-field')).show();
			}
		});

		$('.fun-field').each(function(){

			var ctx = $(this);

			if( ctx.find('ul').length ) {

				if( $(window).width() > 1000 ) {

					// ctx.css({
					// 	width: ctx.find('ul').width()
					// });
					//
					// ctx.find('span').css({
					// 	width: ctx.find('ul').width()
					// });

				} else {

					// if (ctx.find('select').prop('selectedIndex') === 0) {
					// 	$('span',ctx).addClass('empty');
					// } else {
					// 	$('span',ctx).removeClass('empty');
					// }
					//
					// var txt = [];
					// $('option:selected',ctx).each(function(){
					// 	txt.push($(this).text());
					// });
					// $('span',ctx).text(txt.join(', '));
					//
					// ctx.find('select').on('change', function() {
					// 	$('span', $(this).closest('.fun-field')).removeClass('empty');
					// 	var txt = [];
					// 	$('option:selected',$(this).closest('.fun-field')).each(function(){
					// 		txt.push($(this).text());
					// 	});
					// 	$('span',$(this).closest('.fun-field')).text(txt.join(', '));
					// });

				}
			}

			$('li',ctx).click(function(e){
				e.stopPropagation();
				e.preventDefault();

				var front,back;
				var options = [];

				$(this).toggleClass('active');

				$('li.active',ctx).each(function(){
					options.push($(this).text());
				});

				var dis = options.join(', ');

				if (options[0].indexOf('-') > -1) {
					front = options[0].split('-')[0];
					back = (options[options.length-1].indexOf('-') > -1) ? options[options.length-1].split('-')[1] : options[options.length-1].replace('$','');
					dis = front+'-'+back;
				}

				if (options[0].indexOf('–') > -1) {
					front = options[0].split('–')[0];
					back = (options[options.length-1].indexOf('–') > -1) ? options[options.length-1].split('–')[1] : options[options.length-1].replace('$','');
					dis = front+'–'+back;
				}

				var i, usebdr = false;
				for ( i in options ) {
					if (options[i].indexOf('bedroom') > -1) {
						usebdr = true;
					}
				}

				if(usebdr) {
					var newopts = [];
					var ph = null;
					for ( i in options ) {
						if (options[i].indexOf('penthouse') < 0) {
							newopts.push(options[i].replace(' bedroom',''));
						} else {
							ph = 'ph';
						}
					}
					dis = newopts.join(', ') + ' bedroom' + ( ph ? ', ph' : '');
				}

				$('span',$(this).closest('.fun-field')).text(dis).removeClass('error');
				return false;
			});

			if(ctx.find('ul').length === 0 ) {
				$('span',ctx).attr('contentEditable',true);
			}

			$('span',ctx)
				.attr('contentEditable',true)
				.click(openFunFieldOptions)
				.focus(openFunFieldOptions)
				.keydown(function(e){
					var index = 0, active = null;
					switch(e.which){
						case 9: // tab
							closeFunFieldOptions();
							break;
						case 40: //down
							index = $('li.active',$(this).closest('.fun-field')).index();
							index = index < 0 ? 0 : (index >= ($('li',$(this).closest('.fun-field')).length-1)) ? 0 : index+1;
							active = $('li:eq('+index+')',$(this).closest('.fun-field')).addClass('active');
							active.siblings().removeClass('active');
							$(this).text(active.text());
							break;
						case 38: // up
							index = $('li.active',$(this).closest('.fun-field')).index();
							index = index < 0 ? $('li',$(this).closest('.fun-field')).length-1 : (index <= 0 ? ($('li',$(this).closest('.fun-field')).length-1) : index-1 );
							active = $('li:eq('+index+')',$(this).closest('.fun-field')).addClass('active');
							active.siblings().removeClass('active');
							$(this).text(active.text());
							break;
					}
					return e.which != 13;
				});
		});
	});

	$(document).click(closeFunFieldOptions);

	$('#thebutton').click(function(){
		var hasTouch = 'ontouchstart' in window;
		closeFunFieldOptions();
		$(this).val('processing').attr('disabled', true).removeClass('error').addClass('disabled');

		var errors = [];
		var data = [];
		var contact_fields;

		if(!hasTouch || $(window).width() > 736) {
			contact_fields = $('#contact-fields').data('fields').split('|');
		} else {
			contact_fields = $('#contact-fields').data('fields-mobile').split('|');
		}

		for( var f in contact_fields ) {

			var parts = contact_fields[f].split(':');
			var el, value, description;

			// if(!hasTouch || $(window).width() > 736) {

				el = $('[data-id='+parts[0]+']').removeClass('error');
				if ( el.is('input') ) {
					value = el.val();
				} else if ( el.is('span') ) {
					value = el.text();
				}
				description = el.data('field-description')||'';
				if( parts.length>1 && parts[1]=="required" && value.length<=0 ) {
					errors.push(el);
				} else {
					data.push({
						key:parts[0].replace(/\-mobile/,''),
						value:value,
						label:description
					});
				}

			// } else {
			//
			// 	el = $('input[data-id="'+parts[0]+'"], select[data-id="'+parts[0]+'"] option:selected').removeClass('error');
			// 	value = el.val() || el.text();
			// 	description = el.data('field-description')||'';
			// 	if( (parts.length>1&&parts[1]=="required"&&value.length<=0) || (parts.length>1&&parts[1]=="required" && $('select[data-id="'+parts[0]+'"]').prop('selectedIndex') === 0)) {
			// 		errors.push(el);
			// 	} else {
			// 		data.push({
			// 			key:parts[0],
			// 			value:value,
			// 			label:description
			// 		});
			// 	}
			//
			// }
		}

		if( errors.length > 0 ) {

			$(this).attr('disabled', false).removeClass('disabled').addClass('error').val('missing some info');
			for( var i in errors ){
				errors[i].addClass('error');
				errors[i].siblings('label').addClass('error');
				errors[i].parents().siblings('.text').addClass('error');
			}

		} else {

			$(this).attr('disabled', false);

			// setTimeout(function(){
			// 	$(this).attr('disabled', false).removeClass('disabled').val('submit');
			// 	//window.location.href="/thanks";
			// },1000);

			sendContactForm(data);

		}

	});

/*	scaleform();

	$(window).resize(function() {
		scaleform();
	});

	function scaleform(){

		var original_width = 1280;

		if($(window).width() < 1280 && $(window).width() > 1000) {

			$('#contact-fields').css({
				'-ms-transform': 'scale('+ $(window).width() / original_width+ ')',
				'-webkit-transform': 'scale('+ $(window).width() / original_width +')',
				'transform': 'scale('+ $(window).width() / original_width +')'
			});

			$('.fun-field, .fun-field span.text').css({
				'min-width': 320 * ($(window).width() / original_width)+ 'px',
			});

		} else {

			$('#contact-fields').removeAttr('style');

		}
	} */

})(jQuery);
